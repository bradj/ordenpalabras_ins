#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(Prim: str, Sec: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    lower = False
    for pos in range(len(Prim)):
        if pos < len(Sec):
            if Prim[pos].lower() < Sec[pos].lower() :
                lower = True
                break
            if Prim[pos].lower() > Sec[pos].lower() :
                break
    return lower

def sort_pivot(words: list, pos: int):
    """Sorts word in pivot position, moving it to the left until its place"""
    lower: int = pos
    for a in range(pos+1, len(words)) :
        if is_lower(words[a], words[lower]) :
            a, lower = lower, a
    return lower

def sort(words: list):
    """Return the list of words, ordered alphabetically"""
    for pivot in range(len(words)):
        lower = sort_pivot(words, pivot)
        if lower != pivot:
            words[lower], words[pivot] = words[pivot], words[lower]
    return words


def show(words: list):
    """Show words on screen, using print()"""
    for word in words:
        print(word, end=' ')
    print()

def main():
    words: list = sys.argv[1:]
    sort(words)
    show(words)

if __name__ == '__main__':
    main()
